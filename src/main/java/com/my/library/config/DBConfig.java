package com.my.library.config;

import com.my.library.dao.UserDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.my.library.config.DBConstants.*;

@Configuration
public class DBConfig{

    @Bean
    @Scope("singleton") //prototype session
    public Connection connection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB_NAME,
                USER,
                PASSWORD);
    }

    //можно без бина через аннотацию к UserDaoImpl
    @Bean
    @Scope("prototype")
    public UserDaoImpl userDaoImpl() throws SQLException {
        return new UserDaoImpl(connection());
    }
}

