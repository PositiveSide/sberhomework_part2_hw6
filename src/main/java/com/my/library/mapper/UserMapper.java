package com.my.library.mapper;

import com.my.library.model.User;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class UserMapper {

    public User userMapper(ResultSet resultSet) throws SQLException {
        User user = new User();
        user.setId((long) resultSet.getInt("id"));
        user.setLastName(resultSet.getString("last_name"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setTelephoneNumber(resultSet.getString("telephone_number"));
        user.setBirthday(resultSet.getDate("birthday"));
        user.setBookList(resultSet.getString("book_list"));
        return user;
    }

}

