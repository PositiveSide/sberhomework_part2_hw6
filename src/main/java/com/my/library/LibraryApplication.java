/*
1. Создать таблицу клиента с полями: ● Фамилия ● Имя ● Дата рождения ● Телефон ● Почта ● Список названий книг
из библиотеки
2. Создать класс UserDAO и Бин (prototype) этого класса.
3. Далее нам необходимо заполнить пользователей (т.е. у нас должен быть методвкоде,
который позволяет добавить пользователей в базу)
4. Написать метод, который принимает телефон/почту (на ваше усмотрение),
достанетизUserDAO список названий книг данного человека. С этим списком сходить в BookDAO
и получить всю информацию об этих книгах
 */

package com.my.library;

import com.my.library.dao.BookDao;
import com.my.library.dao.UserDao;
import com.my.library.model.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.Date;
import java.util.List;

@SpringBootApplication
public class LibraryApplication implements CommandLineRunner {
    private final UserDao userDao;
    private final BookDao bookDao;

    public LibraryApplication(UserDao userDao, BookDao bookDao) {
        this.userDao = userDao;
        this.bookDao = bookDao;
    }
    public static void main(String[] args) {
        SpringApplication.run(LibraryApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        //пример добавления юзера (полное наполнение базы в sql файле
        userDao.addUser("фам3", "имя3", "999",
                "2020-12-01", "book31, book32, book33");

        //получаем список книг пользователя
        List<String> books = userDao.findAllBookOfUserByTelephone("111");

        //выводим информацию о книгах пользователя
        for (String book: books) {
            System.out.println(bookDao.findBookByTitle(book));
        }
    }
}

