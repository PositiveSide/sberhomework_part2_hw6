package com.my.library.dao;

import com.my.library.mapper.BookMapper;
import com.my.library.model.Book;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class BookDao {

    private BookMapper bookMapper = new BookMapper();
    private final Connection connection;

    public BookDao(Connection connection) {
        this.connection = connection;
    }

    public Book findById(Long id) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("select * from books where id = ?");
        preparedStatement.setInt(1, Math.toIntExact(id));
        ResultSet resultSet = preparedStatement.executeQuery();
        Book book = new Book();
        while (resultSet.next()) {
            book = bookMapper.bookMapper(resultSet);
        }
        return book;
    }

    public Book findBookByTitle(String title) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("select * from books where title = ?");
        preparedStatement.setString(1, title);
        ResultSet resultSet = preparedStatement.executeQuery();
        Book book = new Book();
        while (resultSet.next()) {
            book = bookMapper.bookMapper(resultSet);
        }
        return book;
    }

    public List<Book> findAll() throws SQLException {
        List<Book> books = new ArrayList<>();
        PreparedStatement preparedStatement = connection.prepareStatement("select * from books");
        ResultSet resultSet = preparedStatement.executeQuery();
        books.addAll(bookMapper.bookMapperFromList(resultSet));
        return books;
    }

}
