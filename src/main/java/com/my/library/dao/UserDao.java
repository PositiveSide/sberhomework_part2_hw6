package com.my.library.dao;

import com.my.library.model.Book;
import com.my.library.model.User;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

public interface UserDao {


    User findById(Long id) throws SQLException;

    void addUser(String lastName, String firstName, String telephone, String birthday, String bookList)
            throws SQLException;

    List<String> findAllBookOfUserByTelephone(String telephoneNumber) throws SQLException;
    //    List<Book> findAllBookOfUserByTelephone(String telephoneNumber) throws SQLException;
}

