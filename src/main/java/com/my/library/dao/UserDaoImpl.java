package com.my.library.dao;

import com.my.library.mapper.UserMapper;
import com.my.library.model.User;

import java.sql.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//@Repository
public class UserDaoImpl implements UserDao {
//для варианта через аннотацию без прямого создания бина в DBConfig
//    private final Connection connection;
//    private final UserMapper userMapper;
//    public UserDaoImpl(Connection connection, UserMapper userMapper) {
//        this.connection = connection;
//        this.userMapper = userMapper;
//    }

    private final Connection connection;
    public UserMapper userMapper = new UserMapper();

    public UserDaoImpl(Connection connection) {
        this.connection = connection;
    }
// не требуется для задания
    public User findById(Long id) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "select * from users where id = ?");
        preparedStatement.setInt(1, Math.toIntExact(id));
        ResultSet resultSet = preparedStatement.executeQuery();

        User user = new User();
        while (resultSet.next()) {
            user = userMapper.userMapper(resultSet);
        }
        return user;
    }

    public void addUser(String lastName, String firstName, String telephone, String birthday, String bookList)
            throws SQLException {
        Statement statement = connection.createStatement();
        String sql = String.format("insert into users (last_name, first_name, telephone_number, birthday, book_list)" +
                        "values ('%s', '%s', '%s', '%s','%s')",
                lastName, firstName, telephone, Date.valueOf(birthday), bookList);
        statement.executeUpdate(sql);
        //второй вариант
//        PreparedStatement preparedStatement = connection.prepareStatement(
//                "insert into users (last_name, first_name, telephone_number, birthday, book_list) " +
//                        "values (?,?,?,?,?)");
//        preparedStatement.setString(1, lastName);
//        preparedStatement.setString(2, firstName);
//        preparedStatement.setString(3, telephone);
//        preparedStatement.setDate(4, Date.valueOf(birthday));
//        preparedStatement.setString(5, bookList);
//
//        preparedStatement.executeUpdate();

    }

    public List<String> findAllBookOfUserByTelephone(String telephoneNumber) throws SQLException {
        List<String> books = new ArrayList<>();
        //извлекаем список книг юзера
        PreparedStatement preparedStatement = connection.prepareStatement(
                "select * from users where telephone_number = ?");
        preparedStatement.setString(1, telephoneNumber);
        ResultSet resultSet = preparedStatement.executeQuery();
        //преобразовываем список книг в лист книг
        while (resultSet.next()) {
            String strList = resultSet.getString("book_list");
            books.addAll(Arrays.asList(strList.split(", ")));
        }
        return books;
    }

}


/* это я оставил себе для доработки идеи
    public List<Book> findAllBookOfUserByTelephone(String telephoneNumber) throws SQLException {
        List<String> booksTmp = new ArrayList<>();
        //извлекаем список книг юзера
        PreparedStatement preparedStatement = connection.prepareStatement(
                "select * from users where telephone_number = ?");
        preparedStatement.setString(1, telephoneNumber);
        ResultSet resultSet = preparedStatement.executeQuery();
        //преобразовываем список книг в лист книг
        while (resultSet.next()) {
            String strList = resultSet.getString("book_list");
            booksTmp.addAll(Arrays.asList(strList.split(", ")));
        }
        //получаем информацию по книгам юзера
        List<Book> books = new ArrayList<>();

        for (String bookStr: booksTmp) {
            BookDao.findBookByTitle(bookStr);

//            preparedStatement = connection.prepareStatement("select * from books where title = ?");
//            preparedStatement.setString(1, bookStr);
//            resultSet = preparedStatement.executeQuery();
//            books.add(resultSet);
        }


//        System.out.println(resultSet);
//        books.addAll(bookMapper.bookMapperFromList(resultSet));
        return books;
    }

}
 */

