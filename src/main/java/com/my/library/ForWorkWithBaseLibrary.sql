create table users
(
    id               serial primary key,
    last_name          varchar(30) not null,
    first_name             varchar(30) not null,
    telephone_number varchar(10) not null,
    birthday         date default now(),
    book_list        varchar(300)
);

insert into users (last_name, first_name, telephone_number, birthday, book_list)
values ('фам1', 'имя1', '111', '2022-01-01', 'book1, book2, book3');
insert into users (last_name, first_name, telephone_number, birthday, book_list)
values ('фам2', 'имя2', '222', '2022-02-02', 'book21, book22, book23');
insert into users (last_name, first_name, telephone_number, birthday, book_list)
values ('фам3', 'имя3', '333', '2022-03-03', 'book31, book32, book33');

create table books
(
    id        serial primary key,
    title     varchar(30) not null,
    author    varchar(30) not null,
    date_added date default now()
);

insert into books (title, author)
values ('название1', 'автор1');

insert into books (title, author)
values ('book1', 'автор1');
insert into books (title, author)
values ('book2', 'автор2');
insert into books (title, author)
values ('book3', 'автор3');

insert into books (title, author)
values ('book21', 'автор21');
insert into books (title, author)
values ('book22', 'автор22');
insert into books (title, author)
values ('book23', 'автор23');

insert into books (title, author)
values ('book31', 'автор31');
insert into books (title, author)
values ('book32', 'автор32');
insert into books (title, author)
values ('book33', 'автор33');


drop table users;
drop table books;

SELECT
    book_list FROM users WHERE users.telephone_number LIKE '111';