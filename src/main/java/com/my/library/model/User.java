package com.my.library.model;


import lombok.*;

import java.util.Date;

//@Getter
//@Setter
//@AllArgsConstructor
//@NoArgsConstructor
//@ToString
@Data

public class User {
    private Long id;
    private String lastName;
    private String firstName;
    private String telephoneNumber;
    private Date birthday;
    private String bookList;
}

